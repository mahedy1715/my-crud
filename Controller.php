<?php
include ('Database.php');
use Database as DB;

Class Controller extends  DB{


    private $Name ,  $Email, $Phone, $id;

    public function setData($postData) {


        if(array_key_exists('name',$postData)){$this->Name=$postData['name'];}
        if(array_key_exists('email',$postData)){$this->Email=$postData['email'];}
        if(array_key_exists('phone',$postData)){$this->Phone=$postData['phone'];}
        if(array_key_exists('id',$postData)){$this->id=$postData['id'];}

    }
################################################ Messsage Handling ###############################################
    public  function message($message=NULL)
    {
        if (is_null($message)) {
            $_message = self::getMessage();
            return $_message;
        } else {
            self::setMessage($message);
        }
    }

    public  function setMessage($message){
        $_SESSION['message']=$message;

    }
    public  function getMessage(){
        if(isset($_SESSION['message'])) $_message= $_SESSION['message'];
        else $_message='';

        $_SESSION['message']="";
        return $_message;

    }
################################################ Messsage Handling ###############################################
################################################ Utility Handling ###############################################
    public function d($data){
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    public function dd($data){
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        die();
    }


    public function redirect($data){
        header('Location:'.$data);

    }

################################################ Utility Handling ###############################################


    public function store(){

        $arrData = array($this->Name,$this->Email,$this->Phone);



        $sql = "INSERT INTO  mycrud(Name, Email , Phone)  VALUES(?,?,?)";

        $STH = $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);


        if ($result) {
            $this-> message("Success! You have  successfully registered:)");

        }
        else
            $this->message("Failed! Data Has Not Been Inserted :( ");
            $this->redirect('form.php');
    }

    public function view(){
        $sql ="SELECT * FROM  mycrud";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function singleView(){
        $sql ="SELECT * FROM  mycrud WHERE id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function objectToArray($objectData){
        $objectToArray = json_decode(json_encode($objectData), True);
        return $objectToArray;
    }

    public function edit(){
        $arrData = array($this->Name,$this->Email,$this->Phone);
        //var_dump($arrData);
        $sql = "UPDATE mycrud SET Name=?,Email=?,Phone=? WHERE id=".$this->id;
        //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);


        if ($result) {
            $this-> message("Success! You have  successfully updated:)");
        }
        else
            $this->message("Failed! Data Has Not Been updated :( ");

        $this->redirect('viewdata.php');

    }


    public function delete(){

        $sql = "DELETE from mycrud WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if ($result) {
            $this-> message("Success! You have  successfully deleted:)");
        }
        else
            $this->message("Failed! Data Has Not Been Deleted :( ");

        $this->redirect('viewdata.php');

    }
}

?>